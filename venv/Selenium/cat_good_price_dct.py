from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException
import time

driver = webdriver.Chrome('chromedriver')

url = "http://ovaldancewear.ru/page10779269.html"
# driver.implicitly_wait(10)
driver.get(url)
cats_dct = {}

try:
    # ignored_exceptions = (NoSuchElementException, StaleElementReferenceException)
    # cats = WebDriverWait(driver, 20, ignored_exceptions=ignored_exceptions).until(
    #     EC.presence_of_all_elements_located((By.CLASS_NAME, "js-store-parts-switcher")))

    cats = driver.find_elements_by_class_name("js-store-parts-switcher")

    ignore_lst = ['Все', '', 'All']
    for cat in cats:
        if cat.text not in ignore_lst:
            cat.click()
            time.sleep(1)
            items = driver.find_elements_by_class_name("js-store-prod-name")
            prices = driver.find_elements_by_class_name("js-product-price")
            cats_dct[cat.text] = {item.text:int(price.text.replace(" ","")) for item, price in zip(items, prices)
                                  if item.text != '' and price.text != ''}
    
finally:
    # print(cats_dct)
    driver.quit()
