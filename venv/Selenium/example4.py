from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait


def page_is_loading(driver):
    while True:
        x = driver.execute_script("return document.readyState")
        if x == "complete":
            return True
        else:
            yield False


driver = webdriver.Chrome('chromedriver.exe')

url = "http://ovaldancewear.ru"

driver.get(url)

menu_item = driver.find_element_by_link_text("МАГАЗИН")
menu_item.click()

category_items_bar = driver.find_element_by_class_name("t-store__parts-switch-wrapper")
category_items = category_items_bar.find_elements_by_tag_name('div')
category_num = len(category_items)

ignore_list = ['', "Все", "All"]
for count in range(category_num):
    category_items_bar = driver.find_element_by_class_name("t-store__parts-switch-wrapper")
    category_items = category_items_bar.find_elements_by_tag_name('div')
    for category_item in category_items:
        if category_item.text not in ignore_list:
            category_item.click()

            items = driver.find_elements_by_class_name("js-store-prod-name")

            names_list = []
            for item in items:
                print(item.text)
                # names_list.append(item.text)    # тут иногда ошибка

            # print(names_list)

driver.quit()
