from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait


def page_is_loading(driver):
    while True:
        x = driver.execute_script("return document.readyState")
        if x == "complete":
            return True
        else:
            yield False

driver = webdriver.Chrome('chromedriver')

url = "http://ovaldancewear.ru/page10779269.html?s_recid=182857235&s_storepartuid=%D0%A2%D0%BE%D0%BF%D1%8B+%D0%B8+%D1%81%D0%B2%D0%B8%D1%82%D1%88%D0%BE%D1%82%D1%8B"

driver.get(url)

try:
    # element = WebDriverWait(driver, 10).until(
    #     EC.presence_of_all_elements_located((By.CLASS_NAME, "js-store-prod-name"))
    # )

    while not page_is_loading(driver):
        continue

    items = driver.find_elements_by_class_name("js-store-prod-name")

    names_list = []
    for item in items:
        names_list.append(item.text)

    print(names_list)
    # ['Свитшот с капюшоном из футера “перья”', 'Свитшот комбинированный трехполосный', 'Короткий розовый топ из пайеток', 'Топ зеленый с вырезом “качели”', 'Топ цвета фуксии', 'Топ разноцветный короткий из вискозы', 'Короткий разноцветный топ из пайеток', 'Топ короткий с открытой спиной', 'Топ на бретелях', 'Топ белый короткий', 'Топ бирюзовый короткий', 'Свитшот из футера "пятнышки" серый', 'Топ расклешенный на бретелях', 'Топ серый в горошек', 'Топ цвета лайм', 'Топ черный с перемычкой', 'Топ белый с открытыми плечами', 'Топ васильковый', 'Свитшот из футера "пятнышки"', 'Топ двухцветный с открытыми плечами', '']
finally:
    driver.close()