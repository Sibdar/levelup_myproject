from selenium import webdriver
import time
import json

cats_dct = {}

def get_info():
    driver = webdriver.Chrome('chromedriver')

    url = "http://ovaldancewear.ru/page10779269.html"
    # driver.implicitly_wait(10)
    driver.get(url)
    # cats_dct = {}
    global cats_dct

    cats = driver.find_elements_by_class_name("js-store-parts-switcher")

    ignore_lst = ['Все', '', 'All']
    for cat in cats:
        if cat.text not in ignore_lst:
            cat.click()
            time.sleep(1)
            items = driver.find_elements_by_class_name("js-store-prod-name")
            prices = driver.find_elements_by_class_name("js-product-price")
            cats_dct[cat.text] = {str(item.text.replace('"', 'ˮ')):int(price.text.replace(" ","")) for item, price in zip(items, prices)
                                  if item.text != '' and price.text != ''}
    driver.quit()
    return cats_dct

get_info()


if __name__ == '__main__':
    with open('data.json', 'w', encoding='utf-8') as fp:
        json.dump(cats_dct, fp, indent=4, sort_keys=False, ensure_ascii=False)
