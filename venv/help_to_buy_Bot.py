# API (общее):
# - Посмотреть список товаров (наименование, цена, количество).
# - Получить название магазина.
# - Получить самый дешевый товар.
# - Получить товар с самым большим/маленьким остатком на складе.
# - Получить все товары из определенной категории.

# API (покупателя):
# - Покупать по одному товару в любом (доступном) количестве. Если в наличии нет нужного количества
#   товара, то будет выведено сообщение о том сколько товаров в наличии.
# - Покупать товары по “списку покупок” (файл).
# - Посмотреть свойства товара.

import telebot
token = '1364619768:AAFVaQjx-PuH0YHnxmwmhUCDH0AGIqZ_pQg'
bot = telebot.TeleBot(token)

# COMMON API

# convert dict to string
def convert_to_str(dict):
    return str(dict)

# get the list of goods
@bot.message_handler(commands=['get_list_of_goods'])
def list_of_goods(message):
    bot.send_message(message.chat.id, store_warehouse.get_list_of_goods())

# get the store name
@bot.message_handler(commands=['get_store_name'])
def get_store_name(message):
    bot.send_message(message.chat.id, store_warehouse.get_store_name())

# get the chipest good
@bot.message_handler(commands=['get_chipest_good'])
def get_chipest_good(message):
    bot.send_message(message.chat.id, store_warehouse.get_chipest_good())

# get the goods with most/least number on warehouse
@bot.message_handler(commands=['get_goods_edge_quans'])
def get_goods_edge_quans(message):
    bot.send_message(message.chat.id, f'Товары с наим. кол-ом:\n{store_warehouse.get_least_quantity_good()}\n\n'
                                      f'Товары с наиб. кол-ом:\n{store_warehouse.get_most_quantity_good()}')

id_cat = {}

# get goods by category
@bot.message_handler(commands=['get_goods_by_category'])
def get_goods_by_category(message):
    global id_cat
    markup = types.InlineKeyboardMarkup()
    # for num, cat in enumerate(store_warehouse.list_of_goods):
    #     markup.add(types.InlineKeyboardButton(text=cat, callback_data=num + 1))
    markup.add(*[types.InlineKeyboardButton(text=cat, callback_data=num+1) for num, cat in enumerate(store_warehouse.list_of_goods)])
    id_cat = {num+1:cat for num, cat in enumerate(store_warehouse.list_of_goods)}

    bot.send_message(message.chat.id, 'Выберите категорию: ', parse_mode='html', reply_markup=markup)


@bot.callback_query_handler(func=lambda call: True)
def query_handler(call):
    global id_cat
    bot.answer_callback_query(callback_query_id=call.id, text='Одну минуточку...')
    answer = store_warehouse.get_goods_by_cat(id_cat[int(call.data)])
    bot.send_message(call.message.chat.id, answer)


# CUSTOMERS' API

# but one good instance, any amount
@bot.message_handler(commands=['buy_good'])
def buy_good(message):
    bot.send_message(message.chat.id, "The good is bought")

# buy list of goods (file)
@bot.message_handler(commands=['buy_list_of_goods'])
def buy_list_of_goods(message):
    bot.send_message(message.chat.id, "The list of goods is bought")

# get_good_features
@bot.message_handler(commands=['get_good_features'])
def get_good_features(message):
    bot.send_message(message.chat.id, "Lood at good's features")

if __name__ == '__main__':
    print('Starting Bot...')
    bot.polling(none_stop=True, interval=0)
