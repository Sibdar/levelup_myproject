# API (общее):
# - Посмотреть список товаров (наименование, цена, количество).
# - Получить название магазина.
# - Получить самый дешевый товар.
# - Получить товар с самым большим/маленьким остатком на складе.
# - Получить все товары из определенной категории.

# API (менеджера):
# - Добавить товар по одному наименованию за раз.
# - Добавлять товары списком (файл).
# - Получать / Изменять процент наценки на товары (прибыль магазина).
# - Изменить название магазина.
# - Отобразить статистику (график) покупок.
# - Отобразить статистику (график) использования различных интерфейсов.
from Store.store import store_warehouse
from telebot import types
import telebot
token = '1301935287:AAE_eZr4NyEkLWDAilYv-ZPHZleghSSqOlo'
bot = telebot.TeleBot(token)


# COMMON API

# convert dict to string
def convert_to_str(dict):
    return str(dict)

# get the list of goods
@bot.message_handler(commands=['get_list_of_goods'])
def list_of_goods(message):
    bot.send_message(message.chat.id, store_warehouse.get_list_of_goods())

# get the store name
@bot.message_handler(commands=['get_store_name'])
def get_store_name(message):
    bot.send_message(message.chat.id, store_warehouse.get_store_name())

# get the chipest good
@bot.message_handler(commands=['get_chipest_good'])
def get_chipest_good(message):
    bot.send_message(message.chat.id, store_warehouse.get_chipest_good())

# get the goods with most/least number on warehouse
@bot.message_handler(commands=['get_goods_edge_quans'])
def get_goods_edge_quans(message):
    bot.send_message(message.chat.id, f'Товары с наим. кол-ом:\n{store_warehouse.get_least_quantity_good()}\n\n'
                                      f'Товары с наиб. кол-ом:\n{store_warehouse.get_most_quantity_good()}')

id_cat = {}

# get goods by category
@bot.message_handler(commands=['get_goods_by_category'])
def get_goods_by_category(message):
    global id_cat
    markup = types.InlineKeyboardMarkup()
    for num, cat in enumerate(store_warehouse.list_of_goods):
        markup.add(types.InlineKeyboardButton(text=cat, callback_data=num + 1))
    # markup.add(*[types.InlineKeyboardButton(text=cat, callback_data=num+1) for num, cat in enumerate(store_warehouse.list_of_goods)])
    # id_cat = {num+1:cat for num, cat in enumerate(store_warehouse.list_of_goods)}

    bot.send_message(message.chat.id, 'Выберите категорию: ', parse_mode='html', reply_markup=markup)


@bot.callback_query_handler(func=lambda call: True)
def query_handler(call):
    global id_cat
    bot.answer_callback_query(callback_query_id=call.id, text='Одну минуточку...')
    answer = store_warehouse.get_goods_by_cat(id_cat[int(call.data)])
    bot.send_message(call.message.chat.id, answer)

# MANAGER'S API

# add one good by name
@bot.message_handler(commands=['add_one_good'])
def add_one_good(message):
    bot.send_message(message.chat.id, "One good by name added")

# add list of goods (file)
@bot.message_handler(commands=['add_list_of_goods'])
def add_list_of_goods(message):
    bot.send_message(message.chat.id, "One good by name added")

# get/change % of profit (store's profit) ????
@bot.message_handler(commands=['manage_profit_percentage'])
def manage_profit_percentage(message):
    bot.send_message(message.chat.id, "Getters/Setters")

# change storage name
@bot.message_handler(commands=['change_storage_name'])
def change_storage_name(message):
    bot.send_message(message.chat.id, "Storage name is changed")

# get sales' graphic
@bot.message_handler(commands=['get_sales_graphic'])
def get_sales_graphic(message):
    bot.send_message(message.chat.id, "Open sales graphic")

# get interface's statistic
@bot.message_handler(commands=['get_interface_usage_statistic'])
def get_interface_usage_statistic(message):
    bot.send_message(message.chat.id, "Open interface usage statistic")


if __name__ == '__main__':
    print('Starting Bot...')
    bot.polling(none_stop=True, interval=0)
