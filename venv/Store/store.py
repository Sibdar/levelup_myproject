from Store import warehouse
from Store import good


#def main():
# creating warehouse
store_warehouse = warehouse.Warehouse("Oval Dance Wear")

# creating goods
multicolored_top = good.Good('Топ разноцветный короткий из вискозы', 2250)
short_top_bretel = good.Good('Свитшот с капюшоном из футера "перья"', 2000)
argentino_skirt = good.Good('Юбка для аргентинского танго', 3200)
sunny_skirt_turquoise = good.Good('Юбка "солнце"на запахе в бирюзовых тонах', 3500)
sunny_skirt_cornflower = good.Good('Юбка "солнце" на запахе в васильковых тонах', 3500)
multicolored_shorts = good.Good('Шорты разноцветные из вискозы', 2400)
romb_pants = good.Good('Штаны "Ромб"', 2500)
grey_pants_dotted = good.Good('Брюки серые в горошек', 1900)
grey_leggins = good.Good('Лосины серые', 1700)
multicolored_leggins = good.Good('Лосины разноцветные', 1700)

# extending goods to warehouse
# В будущем, возможно, стоит добавить атрибут "категория" в товар, чтобы можно было добавлять все сразу, а не по
#   одной категории)
store_warehouse.extend_goods_to_store('Топы и свитшоты', 0,  multicolored_top, short_top_bretel)
store_warehouse.extend_goods_to_store('Юбки и платья', 0, argentino_skirt, sunny_skirt_turquoise, sunny_skirt_cornflower)
store_warehouse.extend_goods_to_store('Брюки и лосины', 0, multicolored_shorts, romb_pants, grey_pants_dotted,
                                      grey_leggins, multicolored_leggins)

# добавление кол-ва (товар - сколько)
store_warehouse.add_good_quantity('Топ разноцветный короткий из вискозы', 1)
store_warehouse.add_good_quantity('Топ разноцветный короткий из вискозы', 2)
store_warehouse.add_good_quantity('Юбка для аргентинского танго', 9)
store_warehouse.add_good_quantity('Лосины серые', 11)
store_warehouse.add_good_quantity('Лосины разноцветные', 11)

###################################################################################33

# print(store_warehouse.get_goods_and_quans())
# print(store_warehouse.get_least_quantity_good())
# print(store_warehouse.list_of_goods)
# print(store_warehouse.get_goods_by_cat('Топы и свитшоты'))
# print(store_warehouse.get_most_quantity_good())
# print(store_warehouse.get_least_quantity_good())
# print(store_warehouse.list_of_)

if __name__ == '__main__':
    pass
