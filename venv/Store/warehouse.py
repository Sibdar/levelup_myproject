# API (общее):
# - Посмотреть список товаров (наименование, цена, количество).
# - Получить название магазина.
# - Получить самый дешевый товар.
# - Получить товар с самым большим/маленьким остатком на складе.
# - Получить все товары из определенной категории.


class Warehouse:
    def __init__(self, name):
        self.name = name
        self.list_of_goods = {'Топы и свитшоты': {}, 'Юбки и платья': {}, 'Брюки и лосины': {}}
        self.store_profit = 0.0

    # добавление товаров в магазин (исключительно позиции товара, как таковой)
    def extend_goods_to_store(self, category, quantity, *goods):
        self.list_of_goods[category].update({x:quantity for x in goods})

    # получить список товаров (наименование, цена, кол-во)
    def get_list_of_goods(self):
        list_of_goods_str = ""
        for k,v in self.list_of_goods.items():
            list_of_goods_str += f'{k}:'
            for kk in v:
                list_of_goods_str += f'\n\t* {kk.name}:\n\t\t> Цена:{kk.price}, \n\t\t> Кол-во: {v[kk]}'
            list_of_goods_str += '\n'
        return list_of_goods_str

    # получить товары(классы) и количество
    def get_goods_and_quans(self):
        # global goods_lst
        goods_lst = {}
        for categories, goods_info in self.list_of_goods.items():
            for goods,quants in goods_info.items():
                goods_lst[goods] = quants
        return goods_lst

    # получить товары и цены
    def get_goods_and_prices(self):
        dict_ = {}
        for categories in self.list_of_goods:
            for goods in self.list_of_goods[categories]:
                dict_[goods.get_name()] = goods.get_price()
        return dict_

    # получить название магазина
    def get_store_name(self):
        return self.name

    # добавление кол-ва товара
    def add_good_quantity(self, good_name, quan):
        for k,v in self.list_of_goods.items():
            for kk,vv in v.items():
                if kk.name == good_name:
                    v[kk] = vv + quan

    # получить самый дешевый товар
    def get_chipest_good(self):
        dict_ = {}
        for categories in self.list_of_goods:
            for goods in self.list_of_goods[categories]:
                dict_[goods.get_name()] = goods.get_price()
        return '\n'.join([f'> {x} ({dict_[x]} руб.)' for x in dict_ if dict_[x]==min([dict_[x] for x in dict_])])

    # получить товар с самым большим остатком на складке
    def get_most_quantity_good(self):
        goods = self.get_goods_and_quans()
        return '\n'.join([f'\t> {x.name} ({goods[x]} шт.)' for x in goods
                if goods[x]==max([goods[x] for x in goods])])
        # return [f'> {x.name} ({self.get_goods_and_quans()[x]} шт.)' for x in self.get_goods_and_quans()
        #         if x == max(self.get_goods_and_quans(), key=lambda x:self.get_goods_and_quans()[x])]

    # получить товар с самым маленьким остатком на складке
    def get_least_quantity_good(self):
        goods = self.get_goods_and_quans()
        return '\n'.join([f'> {x.name} ({goods[x]} шт.)' for x in goods
                if goods[x]==min([goods[x] for x in goods])])

    # Получить все товары из определенной категории
    def get_goods_by_cat(self, category):
        list_of_goods_str = ""
        if category in self.list_of_goods:
            list_of_goods_str += f'{category}:'
            lst_g = self.list_of_goods[category]
            for kk in lst_g:
                list_of_goods_str += f'\n\t* {kk.name}:\n\t\t> Цена:{kk.price}, \n\t\t> Кол-во: {lst_g[kk]}'

        # for k, v in self.list_of_goods.items():
        #     if k == category:
        #         list_of_goods_str += f'{k}:'
        #         for kk in v:
        #             list_of_goods_str += f'\n\t* {kk.name}:\n\t\t> Цена:{kk.price}, \n\t\t> Кол-во: {v[kk]}'
        #         list_of_goods_str += '\n'
        return list_of_goods_str










